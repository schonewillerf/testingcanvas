# Testing Canvas

An example usage of the html2canvas library for creating screanshots with JavaScript.

## Usage

Open index.html in any webbrowser

Fill in the form with some random text and click the button to see results

## Example

See screanshot.png for an example

## Purpose

This project was created in order to test how the html2canvas library works.

Now I am considering to use this library in a personal project to render/export a file in the client side


