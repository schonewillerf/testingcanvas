const inputTitle = document.querySelector('#inputTitle');
const inputCoach = document.querySelector('#inputCoach');
const inputClub = document.querySelector('#inputClub');
const inputCompetition = document.querySelector('#inputCompetition');

const documentTitle = document.querySelector('#documentTitle');
const documentCoach = document.querySelector('#documentCoach');
const documentClub = document.querySelector('#documentClub');
const documentCompetition = document.querySelector('#documentCompetition');

const documentSection = document.querySelector('#documentSection');
const canvasSection = document.querySelector('#canvasSection');

const clickButton = document.querySelector('#clickButton');

clickButton.addEventListener( 'click', () => {

    documentTitle.textContent = inputTitle.value;
    documentCoach.textContent = inputCoach.value;
    documentClub.textContent = inputClub.value;
    documentCompetition.textContent = inputCompetition.value;

    html2canvas( documentSection ).then( canvas => canvasSection.appendChild( canvas ) );
})
